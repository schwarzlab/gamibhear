# GAMIBHEAR

## About:
GAMIBHEAR (GAM Incidence Based Haplotype Reconstruction And Estimation) is a graph-based tool for reconstruction of genome-wide haplotypes from Genome Architecture Mapping data. GAMIBHEAR employs GAM-specific proximity scaling to optimize phasing of genomic variants and yields highly accurate chromosome-spanning reconstructed haplotypes.  
With GAMIBHEAR you can easily load your GAM data, clean and prepare it for the subsequent phasing, and choose which of the provided forms of output is most useful for you. You can visualize, compare and process intermediate results, as well as restrict your analysis to target chromosomes or genomic regions of interest and apply custom filters such as individual quality cut offs. The proximity-scaled graph phasing algorithm is time and memory efficiently implemented and parallelized to improve performance.   

For guidelines on how to use the functions provided by this package and more detailed explanations of the workflow please have a look at the [vignette](https://bitbucket.org/schwarzlab/gamibhear/src/master/vignettes/genom_wide_phasing_using_GAM_samples_pdf.pdf).

## Citation:
The full GAMIBHEAR paper is available on biorxiv: [GAMIBHEAR: whole-genome haplotype reconstruction from Genome Architecture Mapping data](https://www.biorxiv.org/content/10.1101/2020.01.30.927061v2).

If you use GAMIBHEAR in your analysis, please cite us.

## Installation
GAMIBHEAR is a user friendly R Package and can be installed using devtools install_bitbucket('schwarzlab/gamibhear').  

## Pre-requisites
GAMIBHEAR was developed and tested for R version 3.4.1.

GAMIBHEAR imports:

* igraph
* reshape2
* Matrix
* vcfR
* parallel
* RcppArmadillo
* Rcpp

## License
GAMIBHEAR is available under GPL-2.

## Maintainer
GAMIBHEAR is being developed and maintained by Julia Markowski (jula.markowski at mdc-berlin.de).